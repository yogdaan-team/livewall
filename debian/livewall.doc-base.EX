Document: livewall
Title: Debian livewall Manual
Author: <insert document author here>
Abstract: This manual describes what livewall is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/livewall/livewall.sgml.gz

Format: postscript
Files: /usr/share/doc/livewall/livewall.ps.gz

Format: text
Files: /usr/share/doc/livewall/livewall.text.gz

Format: HTML
Index: /usr/share/doc/livewall/html/index.html
Files: /usr/share/doc/livewall/html/*.html
